<?php 
require_once("../libs/Smarty.class.php");
require_once("controllers/is_logged.php");
$smarty = new Smarty();
$is_logged = is_logged();

if ( $err = $_SESSION["err"] ) {
	unset($_SESSION["err"]);
	$smarty -> assign("err", $err);
}

if ( $values = $_SESSION["values"] ) {
	unset($_SESSION["values"]);
	$smarty -> assign("values", $values);
}

$smarty -> assign("is_logged", $is_logged);

// Получаем URI для проверки на слеши

$uri = preg_replace("/\?.*/i",'', $_SERVER['REQUEST_URI']);
if (strlen($uri)>1) {// если не главная страница...
	if ( isset($_GET["p"]) ){
		if (rtrim($uri,'/')!=$uri) {
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: http://'.$_SERVER['SERVER_NAME'].str_replace($uri, rtrim($uri,'/'), $_SERVER['REQUEST_URI']));
			exit();    
		}
	}
	else{
		if (rtrim($uri,'/')."/"!=$uri) {
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: http://'.$_SERVER['SERVER_NAME'].str_replace($uri, $uri.'/', $_SERVER['REQUEST_URI']));
			exit();    
		} 
	}
}
if (isset($_GET["page"])){
	switch ($_GET["page"]) {
		case "admin":
			if( $is_logged )
				if ( $is_logged == "admin" ){
					$smarty -> assign("page_title", "Админка");
					$smarty->assign("name", $_SESSION["first_name"]);
					$smarty -> display("includes/adm_header.tpl");
					require_once ("config.php");
					$new_orders = mysql_query("SELECT COUNT(*) FROM orders WHERE status=0");
					$order_count = mysql_fetch_array($new_orders);
					$smarty -> assign("order_count", $order_count[0]);
					$smarty -> display("includes/adm_menu.tpl");
					if ( isset($_GET["action"]) ){
						switch ( $_GET["action"] ){
							case "new_set":
								$smarty -> assign("title", "Добавление нового набора");
								$smarty -> display("includes/new_set.tpl");
							break;
							case "new_product":
								$smarty -> assign("title", "Добавление нового изделия");
								include "config.php";
								$result = mysql_query("SELECT id, article FROM collections ORDER BY id");
								while ($data = mysql_fetch_assoc($result))
									$collections[$data["id"]] = $data["article"];
								$smarty -> assign("collections", $collections);
								$smarty -> assign("selected_collection_id", $values["collections_id"]);
								$type = array(  "1" => "Кольцо",
												"2" => "Серьги",
												"3" => "Браслет",
												"4" => "Колье" );
								$smarty -> assign("type", $type);
								$smarty -> assign("selected_type_id", $values["type_id"]);
								$smarty -> display("includes/new_product.tpl");
							break;
							case "edit_set":
								$smarty -> assign("title", "Редактирование набора");
								if ( isset($_GET['id']) ){
									if ( !$values ){
										include "config.php";
										$result = mysql_query("SELECT
																name,
																article,
																description
															FROM collections
															WHERE id='".$_GET['id']."'");
										$values = mysql_fetch_assoc($result);
									}
									$smarty -> assign("values", $values);
									$smarty -> assign("red", "1");
									$smarty -> display("includes/new_set.tpl");
								}  
							break;
							case "edit_product":
								$smarty -> assign("title", "Редактирование изделия");
								if ( isset($_GET['id']) ){
									if ( !$values ){
										include "config.php";
										$result = mysql_query("SELECT id, article FROM collections ORDER BY article");
										while ($data = mysql_fetch_assoc($result))
											$collections[$data["id"]] = $data["article"];
											$result2 = mysql_query("SELECT article, name, description, price_silver, price_rodiy, price_gold, price_silver2, price_rodiy2, price_gold2, is_size, is_insert_color, type_id, collections_id, status
																	FROM products
																	WHERE id='".$_GET['id']."'");
										$values = mysql_fetch_assoc($result2);
										if ( $values["is_size"] == 1 )
											$values["is_size"] = "checked";
										if ( $values["is_insert_color"] == 1 )
											$values["is_insert_color"] = "checked";
										if ( $values["status"] == 0 )
											$values["status"] = "checked";
									}
									$smarty -> assign("values", $values);
									$smarty -> assign("collections", $collections);
									$smarty -> assign("selected_collection_id", $values["collections_id"]);
									$type = array(  "1" => "Кольцо",
													"2" => "Серьги",
													"3" => "Браслет",
													"4" => "Колье" );
									$smarty -> assign("type", $type);
									$smarty -> assign("red", "1");
									$smarty -> assign("selected_type_id", $values["type_id"]);
									$smarty -> display("includes/new_product.tpl");
								}
								break;
							case "list":
								require_once ("config.php");
								$type = (int) $_GET["type"];
								$num = 30;
								$result_to_nav = mysql_query("SELECT COUNT(*) FROM products");
								require_once ("controllers/nav.php");
								$result = mysql_query("SELECT id, article, name, description, collections_id, type_id, image, is_insert_color, price_silver, price_rodiy, price_gold, status
														FROM products
														ORDER BY id DESC LIMIT ".$start.", ".$num);
								while ( $data = mysql_fetch_assoc($result) ){
									$result2 = mysql_query("SELECT article FROM collections WHERE id = '".$data['collections_id']."'");
									$data2 = mysql_fetch_assoc($result2);
									switch ($data['type_id']){
										case '1': $data['type'] = "Кольцо"; break;
										case '2': $data['type'] = "Серьги"; break;
										case '3': $data['type'] = "Браслет"; break;
										case '4': $data['type'] = "Колье"; break;
									}
									$data['collections'] = $data2['article'];
									$products[$data["id"]] = $data;
								}
								$smarty -> assign("products", $products);
								$smarty -> display("includes/list.tpl");
								break;
							case "sets":
								require_once ("config.php");
								$num = 50;
								$result_to_nav = mysql_query("SELECT COUNT(*) FROM collections");
								require_once ("controllers/nav.php");
								$result = mysql_query("SELECT id, name, article, description
														FROM collections
														ORDER BY id DESC LIMIT ".$start.", ".$num);
								while ( $data = mysql_fetch_assoc($result) )
									$collections[$data["id"]] = $data;
								$smarty -> assign("collections", $collections);
								$smarty -> display("includes/sets.tpl");
								break;
							case "insert_colors":
								$smarty -> assign("title", "Цвета вставок");
								include "config.php";
								$result = mysql_query("SELECT id, name, image, mark_up FROM insert_colors");
								while ( $data = mysql_fetch_assoc($result) )
									$insert_colors[$data["id"]] = $data;
								$smarty -> assign("insert_colors", $insert_colors);
								$smarty -> display("includes/insert_colors.tpl");
								break;
							case "orders":
								require_once ("config.php");
								$type = (int) $_GET["type"];
								$num = 10;
								$result_to_nav = mysql_query("SELECT COUNT(*) FROM orders");
								require_once ("controllers/nav.php");
								$result = mysql_query("SELECT * FROM orders ORDER BY id DESC LIMIT ".$start.", ".$num);
									while ( $data = mysql_fetch_assoc($result) ){
										$result2 = mysql_query("SELECT * FROM users WHERE id = '".$data['users_id']."'");
										$data2 = mysql_fetch_assoc($result2);
										$result3 = mysql_query("SELECT * FROM order_items WHERE orders_id = '".$data['id']."'");
										while (  $data3 = mysql_fetch_assoc($result3) ){
											if(empty($data3['size']))$data3['size'] = "Нет";
											switch ($data3['type']){
												case '1': $data3['type'] = "Кольцо"; break;
												case '2': $data3['type'] = "Серьги"; break;
												case '3': $data3['type'] = "Браслет"; break;
												case '4': $data3['type'] = "Колье"; break;
											}
											$result4 = mysql_query("SELECT * FROM insert_colors WHERE id = '".$data3['insert_color']."'");
											if($data4 = mysql_fetch_assoc($result4))$data3['insert_color'] = $data4['name'];
											else $data3['insert_color'] = "Нет";
											$data['order_items'][$data3['id']] = $data3;
										}
										$data['user'] = $data2;
										$data['date'] = date("Y-m-d H:i:s", $data['date']);
										$pageData[$data['id']] = $data;
									}
									$smarty -> assign("data", $pageData);
									$smarty -> display("includes/orders.tpl");
									break;
								}
							} else {
								$smarty -> display("includes/admin_index.tpl");
							}				
								$smarty -> display("includes/adm_footer.tpl");
				} else echo "Панель администратора. Нет доступа, авторизуйтесь как \"Админ\" <a href='/logout'>Выйти</a>";
			else {
				$smarty -> display("header.tpl");
				$smarty -> display("includes/login.tpl");
				$smarty -> display("footer.tpl");
			}
			break;
		case "order":
			if( $is_logged && isset($values['order_message'])){
				$smarty -> display("header.tpl");
				$smarty -> display("order.tpl");
				$smarty -> display("footer.tpl");
			}
			else{
				header("Location: http://".$_SERVER['HTTP_HOST']); exit();
			}
			break;
		case "products":
			require_once ("config.php");
			$type = (int) $_GET["type"];
			$num = 12;
			$result_to_nav = mysql_query("SELECT COUNT(*) FROM products WHERE type_id='".$type."'");
			switch ($type){
				case '1': $title = "Кольца"; break;
				case '2': $title = "Серьги"; break;
				case '3': $title = "Браслеты"; break;
				case '4': $title = "Колье"; break;
			}
			require_once ("controllers/nav.php");
			$result = mysql_query("SELECT id, article, name, description, image, price_silver, price_rodiy, price_gold, is_size, is_insert_color, type_id
									FROM products
									WHERE type_id='".$type."'
									ORDER BY id LIMIT ".$start.", ".$num);
			while ( $data = mysql_fetch_assoc($result) )
				$products[$data["id"]] = $data;
			$smarty -> assign("products", $products);
			$smarty -> assign("title", $title);
			$smarty -> display("header.tpl");
			$smarty -> display("products.tpl");
			$smarty -> display("footer.tpl");
			break;
		case "sets":
			require_once ("config.php");
			$num = 12;
			$result_to_nav = mysql_query("SELECT COUNT(*) FROM collections");
			require_once ("controllers/nav.php");
			$result = mysql_query("SELECT id FROM collections ORDER BY id LIMIT ".$start.", ".$num);
			while ( $data = mysql_fetch_assoc($result) ){
				$result2 = mysql_query("SELECT id, article, name, description, image, price_silver, price_rodiy, price_gold, is_size, is_insert_color, type_id
										FROM products
										WHERE collections_id='".$data["id"]."'
										ORDER BY id");
				while ( $data2 = mysql_fetch_assoc($result2) ){
					$products[$data["id"]][$data2["id"]] = $data2;
				$set_prices[$data["id"]]['silver'] += $data2['price_silver'];
				$set_prices[$data["id"]]['gold'] += $data2['price_gold'];
				$set_prices[$data["id"]]['rodiy'] += $data2['price_rodiy'];
				}
				$collections[$data["id"]] = $data;
			}
			$smarty -> assign("set_prices", $set_prices);
			$smarty -> assign("products", $products);
			$smarty -> display("header.tpl");
			$smarty -> display("sets.tpl");
			$smarty -> display("footer.tpl");
			break;
		case "password_recovery":
			if ($is_logged) {header("Location: http://".$_SERVER['HTTP_HOST']."/auth/"); exit();}
			if(isset($_GET['hash']) && $_GET['hash'] == $_SESSION["hash"]){
				$smarty -> assign("stage", "new_pass");
				$smarty -> assign("hash", $_GET['hash']);
			}
			else if (isset($_GET['hash'])){
				$values["recovery_message"] = "Не правильная ссылка.";
			}
			$smarty -> assign("values", $values);
			$smarty -> display("header.tpl");
			$smarty -> display("password_recovery.tpl");
			$smarty -> display("footer.tpl");
			break;
		case "basket":
			$smarty -> display("header.tpl");
			$smarty -> display("basket.tpl");
			$smarty -> display("footer.tpl");
			break;
		case "about":
			$smarty -> display("header.tpl");
			$smarty -> display("about.tpl");
			$smarty -> display("footer.tpl");
			break;
		case "contacts":
			$smarty -> display("header.tpl");
			$smarty -> display("contacts.tpl");
			$smarty -> display("footer.tpl");
			break;
		case "pay-and-delivery":
			$smarty -> display("header.tpl");
			$smarty -> display("pay-and-delivery.tpl");
			$smarty -> display("footer.tpl");
			break;
		case "how-to-buy":
			$smarty -> display("header.tpl");
			$smarty -> display("how-to-buy.tpl");
			$smarty -> display("footer.tpl");
			break;
		case "auth":
			$smarty -> display("header.tpl");
			$smarty -> display("index.tpl");
			$smarty -> display("footer.tpl");
			break;
		case "diamonds":
			require_once ("config.php");
			$result = mysql_query("SELECT id, name, image FROM insert_colors");
			while( $data = mysql_fetch_assoc($result) )
				$diamonds[$data['id']] = $data;
			$smarty -> assign("diamonds", $diamonds);
			$smarty -> display("header.tpl");
			$smarty -> display("diamonds.tpl");
			$smarty -> display("footer.tpl");
			break;
		case "metals":
			$smarty -> display("header.tpl");
			$smarty -> display("metals.tpl");
			$smarty -> display("footer.tpl");
			break;
		default:
			$smarty -> display("errors/404.tpl");
	}
}
else if ( isset($_GET["error"]) ){
	switch ($_GET["error"]) {
		case "404": $smarty -> display("errors/404.tpl"); break;
		case "403": $smarty -> display("errors/403.tpl"); break;
		case "401": $smarty -> display("errors/401.tpl"); break;
		case "500": $smarty -> display("errors/500.tpl"); break;
		default: $smarty -> display("errors/404.tpl");
	}
}

else{
	$smarty -> display("header.tpl");
	$smarty -> display("index.tpl");
	$smarty -> display("footer.tpl");
}
?>