<?php 
require ('is_logged.php');
$is_logged = is_logged();
if( $is_logged ){
	header("Location: ".$_SERVER["HTTP_REFERER"]); exit();
}
if( isset($_POST['recover']) ){
    if ( $_POST['email'] != "" ){
        $email = htmlspecialchars(stripslashes(trim($_POST["email"])));
        include ('../config.php');
        $result = mysql_query(" SELECT id, email, first_name, last_name
                                FROM users
                                WHERE email = '".$email."'");
        $data = mysql_fetch_assoc($result);

        if( strtolower($email) == strtolower($data["email"]) ){
            $hash = md5(mt_rand());
            $to = $email;
            $expire = date("H:i d.m.Y",(time() + ini_get("session.gc_maxlifetime")));
            $title = "Восстановление пароля на сайте ".$_SERVER['SERVER_NAME'];
            $letter = "Здравствуйте, ".$data["first_name"]." ".$data["last_name"].".<br />
                       Вы запросили восстановление пароля на сайте ".$_SERVER['SERVER_NAME'].".<br />
                       Для того чтобы задать новый пароль, перейдите по ссылке <a href='".$_SERVER["HTTP_REFERER"]."/".$hash."' и следуйте инструкциям на странице.<br />
                       Ссылка и код восстановления будут активны до $expire.<br />
                       Пожалуйста, проигнорируйте данное письмо, если оно попало к Вам по ошибке.<br />
                       С уважением, администрация сайта ".$_SERVER['SERVER_NAME'].".
                       ";
            $headers = "MIME-Version: 1.0\r\n Content-type: text/html; charset=utf-8";
            if ( mail($to, $title, $letter, $headers) ){
                $_SESSION["user_id"] = $data["id"];
                $_SESSION["hash"] = $hash;
                $values["recovery_message"] = "На почту ".$data["email"]." отправлено письмо с инструкцией для восстановления.";
            }
            else
                $err["recovery"] = "Произошла ошибка отправки почты, попробуйте еще раз или свяжитесь с администратором сайта";
        }
        else{
            $err["recovery"] = "Такого пользователя нет";
        }
        $values["email"] = $_POST["email"];
        $_SESSION["values"] = $values;
    }
    else
        $err["recovery"] = "Введите e-mail";
}
else if( isset($_POST['change_pass']) ){
    if ( $_POST['hash'] == $_SESSION["hash"] ){
        if ( $_POST["new_pass"] == $_POST["new_pass_retype"] ){
            $new_pass = md5(md5($_POST['new_pass']));
            include ('../config.php');
            mysql_query("UPDATE users
                         SET password='".$new_pass."'
                         WHERE id='".$_SESSION["user_id"]."'");
            session_destroy();
            $values["recovery_message"] = "Пароль успешно изменен, перейдите на <a href='/'>главную страницу</a> для авторизации";
            $_SESSION["values"] = $values;
            header("Location: http://".$_SERVER['HTTP_HOST']."/auth/"); exit;
        }
        else
            $err["recovery"] = "Пароли не совнадают";
    }
    else
        $err["recovery"] = "Не правильная ссылка.";
}
$_SESSION["err"] = $err;
$_SESSION["values"] = $values;
header("Location: ".$_SERVER["HTTP_REFERER"]);
?>