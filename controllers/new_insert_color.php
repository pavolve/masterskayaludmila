<?php
require ("is_logged.php");
$is_logged = is_logged();
if ( $is_logged != "admin" ){
	header("Location: http://".$_SERVER["HTTP_HOST"]); exit();
}
include "../config.php";
function is_valid($value, $type = "no"){
	switch ($type) {
		case "name":
        	if (!$value) {
                return "Поле обязательно для заполнения";
            }
			if ( mb_strlen($value, 'utf8') > 32 ){
                $diff = mb_strlen($value, 'utf8') - 32;
                return "Максимально допустимая длина 32 символов( превышено на $diff )";
            }
			break;
		case "image":
			if( $_FILES["image"]["error"] != 0 ){
				switch( $_FILES["image"]["error"] ){ 
					case 1: return "Размер файла превышает ".ini_get('upload_max_filesize'); 
					break; 
					case 2: return "Размер файла превышает допустимый"; 
					break; 
					case 3: return "Загружаемый файл был получен только частично"; 
					break; 
					case 4: return "Файл не был загружен"; 
					break; 
				}
			}
			else{
				$imageinfo = getimagesize($_FILES["image"]["tmp_name"]);
			}
			if($_FILES["image"]["size"] > 1024*2*1024){ 
				return("Размер файла превышает 2 мегабайта"); 
			} 
			if($imageinfo["mime"] != "image/gif" && $imageinfo["mime"] != "image/jpeg" && $imageinfo["mime"] != "image/png") {
				return "Файл не является изображением GIF, JPEG или PNG";
			}
			break;
		default:
			return;
	}
}

if ( isset($_POST["submit"]) ){
    $name = trim(htmlspecialchars($_POST["name"]));
    $mark_up = (int)$_POST["mark_up"];
    if ( $mark_up )
        $mark_up_to_base = ", mark_up='".$mark_up."'";
    $values["mark_up"] = $mark_up;
    if ($error = is_valid($name, "name")){
        if ( isset($_GET["id"]) )
            $err["name"][$_GET["id"]] = "<span class='errMessage'>$error</span>";
        else
            $err["name"][0] = "<span class='errMessage'>$error</span>";
        $values["name"] = $name;
    }
    if ( !empty($_FILES["image"]["name"]) )
        if ( $error = is_valid($_FILES["image"]["name"], "image") ){
            if ( isset($_GET["id"]) )
                $err["image"][$_GET["id"]] = "<span class='errMessage'>$error</span>";
            else
                $err["image"][0] = "<span class='errMessage'>$error</span>";       
        }
    if(!$err){
        if ( isset($_GET["id"]) ){
            $current_id = (int)$_GET["id"];;
            mysql_query("UPDATE insert_colors SET name='".$name."'".$mark_up_to_base." WHERE id = ".$current_id);
        }
        else{
            mysql_query("INSERT INTO insert_colors SET name='".$name."'".$mark_up_to_base."");
            $current_id = mysql_insert_id();
        }
        if( !empty($_FILES["image"]["name"]) ){
            require("func_image_resize.php");
            $expansion = end(explode(".", $_FILES["image"]["name"]));
            $image_name = strtolower($current_id.".".$expansion);
            img_resize($_FILES["image"]["tmp_name"], "../img/diamonds/".$image_name, 60);
            mysql_query("UPDATE insert_colors
                        SET image='".$image_name."'
                        WHERE id='".$current_id."'");
        }
    }
    else{
        $_SESSION["err"] = $err;
    }
    $_SESSION["values"] = $values;
}
header("Location: ".$_SERVER["HTTP_REFERER"]); exit();
?>