<?php 
session_start();
if( isset($_GET["index"]) ){
    $index = $_GET["index"];
    $quantity = $_GET["quantity"];
    $price = $_SESSION["basket"][$index]["cost"];
    $new_price = $price * $quantity;
    $_SESSION["basket"][$index]["quantity"] = $quantity;
    $_SESSION["basket"][$index]["price"] = $new_price;
    foreach ( $_SESSION["basket"] as $item_id => $item ){
        $items_count += $item["quantity"];
        $items_price += $item["price"];
    }
    $_SESSION["basket_count"] = $items_count;
    $_SESSION["basket_price"] = $items_price;
    $new["price"] = $new_price;
    $new["items_count"] = $items_count;
    $new["items_price"] = $items_price;
    echo json_encode($new);
}
?>