<?php
session_start();
if ( isset($_GET["index"]) ){
    unset($_SESSION["basket"][$_GET["index"]]);
}
$items_count = 0;
$items_price = 0;
foreach ( $_SESSION["basket"] as $item ){
    $items_count += $item["quantity"];
    $items_price += $item["price"];
}
$_SESSION["basket_count"] = $items_count;
$_SESSION["basket_price"] = $items_price;
header("Location: ".$_SERVER["HTTP_REFERER"]); exit();
?>