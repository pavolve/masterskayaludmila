<?php    

session_start();
if ( !isset( $_SESSION["basket_index_number"] ) )
    $_SESSION["basket_index_number"] = 0;
require_once("../../config.php");
switch ( $_POST["color"] ){
    case "s":
        $color = "серебро";
        $price_color = "price_silver";
        $price_color2 = "price_silver2";
        break;
    case "g":
        $color = "золото";
        $price_color = "price_gold";
        $price_color2 = "price_gold2";
        break;
    case "r":
        $color = "родий";
        $price_color = "price_rodiy";
        $price_color2 = "price_rodiy2";
        break;    
}
if ( isset($_POST["product_id"]) ){
    $product_id = (int)$_POST["product_id"];
    $result = mysql_query("SELECT id, article, name, ".$price_color.", ".$price_color2.", is_size, is_insert_color, type_id, image
                        FROM products
                        WHERE id='".$product_id."'");
    $product = mysql_fetch_assoc($result);
    $id = $product["id"];
    switch ($product["type_id"]) {
        case "1": $type = "Кольцо"; break;
        case "2": $type = "Серьги"; break;
        case "3": $type = "Браслет"; break;
        case "4": $type = "Колье";
    }
    if ( $product["is_insert_color"] ){
        $insert_color = (int)$_POST["insert_color"];        
        $result2 = mysql_query("SELECT id, name, image, mark_up FROM insert_colors WHERE id='".$insert_color."'");
        $insert_color_arr = mysql_fetch_assoc($result2);
        $mark_up = $insert_color_arr["mark_up"];
        $order_item["insert_color_id"] = $insert_color_arr["id"];
        $order_item["insert_color"] = $insert_color_arr["image"];
    }else{
        $order_item["insert_color"] = "None";
        $order_item["insert_color_id"] = 0;
        $mark_up = 0;
    }
    if ( $product["is_size"] ){
        $size = $_POST["size"];
        $order_item["size"] = $size;
    }
    $order_item["id"] = $id;
    $order_item["type"] = $type;
    $order_item["color"] = $color;
    $order_item["type_id"] = $product["type_id"];
    $order_item["article"] = $product["article"];
    $order_item["name"] = $product["name"];
    $order_item["image"] = $product["image"];
    $order_item["quantity"] = (int)$_POST["quantity"];
	
    if ($mark_up)$order_item["cost"] = (int)$product[$price_color2];
    else $order_item["cost"] = (int)$product[$price_color];
	
    $order_item["price"] = $order_item["cost"]*$order_item["quantity"];
    $items_count = 0;
    if( isset($_SESSION["basket"]) ){
        foreach ( $_SESSION["basket"] as $item_id => $item ){
            if ( $id == $item["id"] && $size == $item["size"] && $order_item["insert_color"] == $item["insert_color"]){
                $_SESSION["basket"][$item_id]["quantity"] += $order_item["quantity"];
                $_SESSION["basket"][$item_id]["price"] += $order_item["price"];
                $added = TRUE;
            }
            $items_count += $item["quantity"];
            $items_price += $item["price"];
        }
    }

    if ( !$added ){
        $_SESSION["basket"][$_SESSION["basket_index_number"]] = $order_item;
        $_SESSION["basket_index_number"] += 1;
    }
    $items_count += $order_item["quantity"];
    $items_price += $order_item["price"];
}

else if ( isset($_POST["set_id"]) ){
    $set_id = (int)$_POST["set_id"];
    $result = mysql_query("SELECT id, article, name, ".$price_color.", ".$price_color2.", is_size, is_insert_color, type_id, image
                            FROM products
                            WHERE collections_id='".$set_id."'");

    while( $product = mysql_fetch_assoc($result) ){
        $order_item = array();
        $added = FALSE;
        $size = "";
        $order_item["insert_color"] = "";
        $id = $product["id"];
        switch ($product["type_id"]) {
            case "1": $type = "Кольцо"; break;
            case "2": $type = "Серьги"; break;
            case "3": $type = "Браслет"; break;
            case "4": $type = "Колье";
        }
        if ( $product["is_insert_color"] != 0 ){
            $insert_color = (int)$_POST["insert_color"];
            $result2 = mysql_query("SELECT id, name, image, mark_up FROM insert_colors WHERE id='".$insert_color."'");
            $insert_color_arr = mysql_fetch_assoc($result2);
            $mark_up = $insert_color_arr["mark_up"];
            $order_item["insert_color_id"] = $insert_color_arr["id"];
            $order_item["insert_color"] = $insert_color_arr["image"];
        }else{
            $order_item["insert_color"] = "None";
            $order_item["insert_color_id"] = 0;
            $mark_up = 0;
        }
        if ( $product["is_size"] != 0 ){
            $size = $_POST["size"];
            $order_item["size"] = $size;
        }        
        $order_item["id"] = $id;
        $order_item["type"] = $type;
        $order_item["color"] = $color;
        $order_item["type_id"] = $product["type_id"];
        $order_item["article"] = $product["article"];
        $order_item["name"] = $product["name"];
        $order_item["image"] = $product["image"];
        $order_item["quantity"] = (int)$_POST["quantity"];
		
        if ($mark_up) $order_item["cost"] = (int)$product[$price_color2];
        else $order_item["cost"] = (int)$product[$price_color];
		
        $order_item["price"] = $order_item["cost"]*$order_item["quantity"];
        $items_count = 0;
        $basket_price = 0;
        if( isset($_SESSION["basket"]) ){
            foreach ( $_SESSION["basket"] as $item_id => $item ){
                if ( $id == $item["id"] && $size == $item["size"] && $order_item["insert_color"] == $item["insert_color"]){
                    $_SESSION["basket"][$item_id]["quantity"] += $order_item["quantity"];
                    $_SESSION["basket"][$item_id]["price"] += $order_item["price"];
                    $added = TRUE;
					$items_count += $item["quantity"];
                	$items_price += $item["price"];
                }
                /**/
            }
        }
        if ( !$added ){
            $_SESSION["basket"][$_SESSION["basket_index_number"]] = $order_item;
            $_SESSION["basket_index_number"] += 1;
        }
        $items_count += $order_item["quantity"];
        $items_price += $order_item["price"];
    }
    $type = "Набор";
}
$_SESSION["basket_count"] = $items_count;
$_SESSION["basket_price"] = $items_price;
echo "<section id='alert'>Добавлено: <b>".$type."</b> ".$order_item["article"]." <b>".$order_item["quantity"]."</b> шт.</section>";

?>