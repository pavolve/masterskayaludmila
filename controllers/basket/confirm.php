<?php 
session_start();
header("Content-Type:text/html; charset=utf-8");
include_once "../../../libs/Smarty.class.php";
include_once "../is_logged.php";

$smarty = new Smarty();
$is_logged = is_logged();

if ( $is_logged ){
    header("Location: http://".$_SERVER["HTTP_HOST"]."/controllers/issue_order.php"); exit();
} else {
    $smarty -> display("file:".$_SERVER[SUBDOMAIN_DOCUMENT_ROOT]."/templates/autorization.tpl");
}
?>
