<?php
require ('is_logged.php');
$is_logged = is_logged();
if($is_logged){
	session_destroy();
	header("Location: ".$_SERVER['HTTP_REFERER'] );
}
else{
	header("Location: http://".$_SERVER["HTTP_HOST"]);
}
?>