<?php
require ("is_logged.php");
$is_logged = is_logged();
if ( $is_logged != "admin" ){
	header("Location: http://".$_SERVER["HTTP_HOST"]); exit();
}
include "../config.php";
function is_valid($value, $type = "no"){
	switch ($type) {
		case "name":
        	/*if (!$value) {
                return "Поле обязательно для заполнения";
            }*/
			if ( strlen($value) > 128 ){
                $diff = strlen($value) - 128;
                return "Максимально допустимая длина 128 символов( превышено на $diff )";
            }
			break;
		case "description":
        	/*if (!$value) {
                return "Поле обязательно для заполнения";
            }*/
			if ( strlen($value) > 255 ){
                $diff = strlen($value) - 255;
                return "Максимально допустимая длина 255 символов( превышено на $diff )";
            }
			break;
		case "article":
        	if (!$value) {
                return "Поле обязательно для заполнения";
            }
            if(strlen($value) != 6){
				return "Артикул должен состоять из 6 символов";
			}
            if( !isset($_POST['submit_red']) ){
                $query = mysql_query("SELECT COUNT(id) FROM products WHERE article='".mysql_real_escape_string($value)."'");
                if(mysql_result($query, 0) > 0){
                    return "Изделие с таким артикулом уже существует";
                }
			}
			break;
		case "collections_id":
            if ( $value && !preg_match("/^[0-9]+$/",$value) )
                return "Выберите коллекцию";
            else return;
			break;
		case "type_id":
            if ( !preg_match("/^[0-9]+$/",$value) )
                return "Выберите тип изделия";
			break;
		case "image":
			if( $_FILES["image"]["error"] != 0 ){
				switch( $_FILES["image"]["error"] ){ 
					case 1: return "Размер файла превышает ".ini_get('upload_max_filesize'); 
					break; 
					case 2: return "Размер файла превышает допустимый"; 
					break; 
					case 3: return "Загружаемый файл был получен только частично"; 
					break; 
					case 4: return "Файл не был загружен"; 
					break; 
				}
			}
			else{
				$imageinfo = getimagesize($_FILES["image"]["tmp_name"]);
			}
			if($_FILES["image"]["size"] > 1024*2*1024){ 
				return("Размер файла превышает 2 мегабайта"); 
			} 
			if($imageinfo["mime"] != "image/gif" && $imageinfo["mime"] != "image/jpeg" && $imageinfo["mime"] != "image/png") {
				return "Файл не является изображением GIF, JPEG или PNG";
			}
			break;
		default:
			return;
	}
}

if ( isset($_POST) ){
    if (isset($_POST)){
        foreach ($_POST as $key => $value){
            $values[$key] = trim(htmlspecialchars($value));
            if ($error = is_valid($values[$key], $key))
                $err[$key] = "<span class='errMessage'>$error</span>";
        }
        if ( $_FILES["image"]["name"] )
            $err["image"] = is_valid($_FILES["image"]["name"], "image");
        if ($err)
            $err = array_diff($err, array(""));
            
        if(!$err){
            $values = array_diff($values, array(""));
            array_pop($values);
            if ( $values["type_id"] != 1 )
                $values["is_size"] = "0";
            else 
                $values["is_size"] = "1";
            if ( !isset($values["is_insert_color"]) )
                $values["is_insert_color"] = "0";
            if ( !isset($values["status"]) )
                $values["status"] = "1";
            foreach ($values as $key => $value) {
                $query .= " ".$key."='".mysql_real_escape_string($value)."',";
            }
            $query = substr($query, 0, -1);
        
        
            if ( !isset($_POST['submit_red']) ){
                mysql_query("INSERT INTO products SET".$query);
                $current_id = mysql_insert_id();
            }
            else{
                $current_id = $_GET["id"];;
                mysql_query("UPDATE products SET".$query." WHERE id = ".$current_id);
            }
            if( !empty($_FILES["image"]["name"]) ){
                
	            require("func_image_resize.php");
                $expansion = end(explode(".", $_FILES["image"]["name"]));
                $image_name = strtolower($current_id.".".$expansion);
                img_resize($_FILES["image"]["tmp_name"], "../files/product_images/".$image_name, 300);
                
                /*
$expansion = end(explode(".", $_FILES["image"]["name"]));
                $image_name = strtolower($current_id.".".$expansion);
				move_uploaded_file($_FILES["image"]["tmp_name"], "../files/product_images/" . $image_name);
*/
                mysql_query("UPDATE products
                            SET image='".$image_name."'
                            WHERE id='".$current_id."'");
            }
            if ( isset($_POST["submit_more"]) ){
                $values = array("type_id" => $_POST["type_id"]);
            }            
            else{
                header("Location: http://".$_SERVER["HTTP_HOST"]."/admin/list"); exit();
            }
        }
        else{
            if ( isset($values["is_size"]) )
                $values["is_size"] = "checked";
            if ( isset($values["is_insert_color"]) )
                $values["is_insert_color"] = "checked";
            if ( isset($values["status"]) )
                $values["status"] = "checked";
                
            $_SESSION["err"] = $err;
        }
        $_SESSION["values"] = $values;
        header("Location: ".$_SERVER["HTTP_REFERER"]); exit();
    }
}

?>