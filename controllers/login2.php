<?php
session_start();
header("Content-Type:text/html; charset=utf-8");
include_once "../../libs/Smarty.class.php";
include_once "is_logged.php";
$smarty = new Smarty();
$is_logged = is_logged();
if( $is_logged ){
	header("Location: ".$_SERVER["HTTP_REFERER"]); exit();
}
if ( empty($_POST["email"]) || empty($_POST["password"]) ){
    $err["login_email"] = "Заполните все поля.";
    
} else {

    $email = htmlspecialchars(stripslashes(trim($_POST["email"])));
    $password = md5(md5(trim($_POST["password"])));
    
    require_once('../config.php');
    
    $result = mysql_query(" SELECT id, email, password, first_name, last_name, permissions
                            FROM users
                            WHERE email = '".$email."'");
    
    if ( $data = mysql_fetch_array($result) )
        if( strtolower($email) == strtolower($data["email"]) ){
            if ( $password == $data["password"]){
                $_SESSION["user_id"] = $data["id"];
                $_SESSION["email"] = $data["email"];;
                $_SESSION["first_name"] = $data["first_name"];
                $_SESSION["last_name"] = $data["last_name"];
                $_SESSION["permissions"] = $data["permissions"];
                echo 0; exit();
            } 
        }
    $err["login_email"] = "E-mail или пароль не верны.";

}
$values["login_email"] = $_POST["email"];
$_SESSION["err"] = $err;
$_SESSION["values"] = $values;
if ( $err = $_SESSION["err"] ) {
    unset($_SESSION["err"]);
    $smarty -> assign("err", $err);
}
if ( $values = $_SESSION["values"] ) {
    unset($_SESSION["values"]);
    $smarty -> assign("values", $values);
}
$smarty -> display("file:".$_SERVER[SUBDOMAIN_DOCUMENT_ROOT]."/templates/autorization.tpl");
?>