<?php
session_start();
header("Content-Type:text/html; charset=utf-8");
include_once "../../libs/Smarty.class.php";
include_once "is_logged.php";
$smarty = new Smarty();
$is_logged = is_logged();
function is_valid($value, $type = "no"){
	if (!$value)
		return "Поле обязательно для заполнения";
    if ( $type == "first_name" or $type == "last_name" )
        if ( strlen($value) > 16 )
            return "Значение не может привышать 16 символов";
	switch ($type) {
		case "password":
            if ( strlen($value) > 32 )
                return "Значение не может привышать 32 символа";
			if(!preg_match("/^[a-zA-Z0-9]+$/",$value))
				return "Пароль может состоять только из букв английского алфавита и цифр";
			break;
		case "retype_password":
			if( trim(htmlspecialchars($_POST["password"])) != $value )
				return "Пароли не совпадают";
			break;
		case "email":
            if ( strlen($value) > 64 )
                return "Значение не может привышать 64 символов";
			$query = mysql_query("SELECT COUNT(id) FROM users WHERE email='".mysql_real_escape_string($value)."'");
			if(mysql_result($query, 0) > 0)
				return "Пользователь с таким e-mail уже существует";
			if (!(preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix",$value)))
				return "E-mail введен неверно";
			break;
        case "phone":
            if ( strlen($value) > 20 )
                return "Значение не может привышать 20 символов";
		default:
			return;
	}
}
if (isset($_POST)){
    include_once "../config.php";
    foreach ($_POST as $key => $value){
        $values[$key] = trim(htmlspecialchars($value));
        if ($error = is_valid($values[$key], $key)){
            $err[$key] = "<span class='errMessage'>$error</span>";
            $err[$key.'_style'] = "style='border:1px solid #f00'";
        }
            
    }
    if(!$err){
        mysql_query("INSERT
                     INTO users
                     SET 
                            email='".$values["email"]."',
                            password='".md5(md5($values["password"]))."',
                            first_name='".$values["first_name"]."',
                            last_name='".$values["last_name"]."',
                            city='".$values["city"]."',
                            phone='".$values["phone"]."'
                     ");
        $current_id = mysql_insert_id();
        $_SESSION["user_id"] = $current_id;
        $_SESSION["email"] = $values["email"];
        $_SESSION["first_name"] = $values["first_name"];
        $_SESSION["last_name"] = $values["last_name"];
        $_SESSION["permissions"] = 0;
        echo 0;
    }
    else{
        $_SESSION["err"] = $err;
        $_SESSION["values"] = $values;
        if ( $err = $_SESSION["err"] ) {
            unset($_SESSION["err"]);
            $smarty -> assign("err", $err);
        }
        if ( $values = $_SESSION["values"] ) {
            unset($_SESSION["values"]);
            $smarty -> assign("values", $values);
        }
        $smarty -> display("file:".$_SERVER[SUBDOMAIN_DOCUMENT_ROOT]."/templates/autorization.tpl");
    }
}
else
    header("Location: ../"); exit();
?>