-- phpMyAdmin SQL Dump
-- version 3.4.11.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 19, 2013 at 02:20 PM
-- Server version: 5.5.30
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `themagj8_jewelry`
--

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

DROP TABLE IF EXISTS `collections`;
CREATE TABLE IF NOT EXISTS `collections` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `article` varchar(6) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;

--
-- Dumping data for table `collections`
--

INSERT INTO `collections` (`id`, `article`, `name`, `description`) VALUES
(11, '100041', '', ''),
(12, '100101', '', ''),
(13, '100141', '', ''),
(14, '100291', '', ''),
(15, '100301', '', ''),
(16, '100381', '', ''),
(17, '100611', '', ''),
(18, '100621', '', ''),
(19, '100781', '', ''),
(20, '100981', '', ''),
(21, '101141', '', ''),
(22, '101191', '', ''),
(23, '101231', '', ''),
(24, '101311', '', ''),
(25, '101401', '', ''),
(26, '101381', '', ''),
(27, '101421', '', ''),
(28, '101461', '', ''),
(29, '101491', '', ''),
(30, '101511', '', ''),
(31, '101521', '', ''),
(32, '101541', '', ''),
(33, '101561', '', ''),
(34, '101601', '', ''),
(35, '101641', '', ''),
(36, '101711', '', ''),
(37, '101911', '', ''),
(38, '101971', '', ''),
(39, '102061', '', ''),
(40, '102071', '', ''),
(41, '102281', '', ''),
(43, '102521', '', ''),
(45, '102541', '', ''),
(46, '102621', '', ''),
(47, '102741', '', ''),
(48, '102961', '', ''),
(49, '103051', '', ''),
(50, '103121', '', ''),
(51, '103141', '', ''),
(52, '103221', '', ''),
(53, '103231', '', ''),
(54, '103241', '', ''),
(55, '103281', '', ''),
(56, '103311', '', ''),
(57, '103331', '', ''),
(58, '103321', '', ''),
(59, '103461', '', ''),
(60, '103441', '', ''),
(61, '103371', '', ''),
(62, '103631', '', ''),
(63, '103571', '', ''),
(64, '103661', '', ''),
(65, '103641', '', ''),
(66, '103711', '', ''),
(67, '104121', '', ''),
(68, '104141', '', ''),
(69, '104251', '', ''),
(70, '104161', '', ''),
(71, '104391', '', ''),
(72, '104411', '', ''),
(73, '104461', '', ''),
(74, '104541', '', ''),
(75, '104651', '', ''),
(76, '104731', '', ''),
(77, '104741', '', ''),
(78, '104851', '', ''),
(79, '104991', '', ''),
(80, '105061', '', ''),
(81, '104650', '', ''),
(82, '102461', '', ''),
(83, '102591', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `insert_colors`
--

DROP TABLE IF EXISTS `insert_colors`;
CREATE TABLE IF NOT EXISTS `insert_colors` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `image` varchar(255) NOT NULL,
  `mark_up` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `insert_colors`
--

INSERT INTO `insert_colors` (`id`, `name`, `image`, `mark_up`) VALUES
(1, 'Белый', '1.png', 0),
(2, 'Лаванда', '2.png', 0),
(3, 'Аметист', '3.png', 0),
(4, 'Желтый', '4.png', 0),
(5, 'Оранжевый', '5.png', 0),
(6, 'Гранат', '6.png', 0),
(7, 'Чёрный', '7.png', 0),
(8, 'Темный перидот (олива)', '8.png', 0),
(9, 'Шампань', '9.png', 0),
(10, 'Розовый', '10.png', 0),
(11, 'Перидот (зеленое яблоко)', '11.png', 0),
(12, 'Красный', '12.png', 0),
(14, 'Коричневый', '14.png', 1),
(15, 'Зеленый', '15.png', 1),
(16, 'Танзанит', '16.png', 1),
(17, 'Голубой топаз', '17.png', 1),
(18, 'Синий', '18.png', 1),
(19, 'Рубин', '19.png', 0),
(20, 'Турмалин', '20.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(8) NOT NULL,
  `date` bigint(20) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_orders_users1_idx` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `users_id`, `date`, `status`) VALUES
(6, 1, 1347021414, 1),
(7, 1, 1347021567, 0),
(8, 21, 1357214313, 0),
(9, 22, 1359127106, 0),
(10, 23, 1359285023, 0),
(11, 23, 1359285690, 0),
(12, 24, 1359556935, 0),
(13, 23, 1359704357, 0),
(14, 25, 1360181692, 0),
(15, 26, 1360852220, 0),
(16, 27, 1362143701, 0),
(17, 23, 1363872946, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `orders_id` int(8) NOT NULL,
  `article` varchar(6) NOT NULL,
  `products_id` int(8) NOT NULL,
  `type` int(1) NOT NULL,
  `quantity` int(8) NOT NULL,
  `size` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `insert_color` varchar(45) DEFAULT NULL,
  `price` varchar(8) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_items_orders1_idx` (`orders_id`),
  KEY `fk_order_items_products1_idx` (`products_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=88 ;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `orders_id`, `article`, `products_id`, `type`, `quantity`, `size`, `color`, `insert_color`, `price`) VALUES
(57, 6, '100040', 120, 1, 1, '17.5', 'золото', '3', '55'),
(58, 6, '100045', 121, 2, 1, '17.5', 'золото', '3', '110'),
(59, 7, '100100', 122, 1, 1, NULL, 'родий', '18', '60'),
(60, 7, '100105', 123, 2, 1, NULL, 'родий', '18', '120'),
(61, 7, '100620', 134, 1, 1, NULL, 'золото', '3', '57'),
(62, 7, '100780', 136, 1, 7, NULL, 'золото', '3', '55'),
(63, 8, '101710', 171, 1, 1, NULL, 'золото', '7', '77'),
(64, 8, '101715', 172, 2, 1, NULL, 'золото', '7', '154'),
(65, 9, '101195', 144, 2, 1, NULL, 'золото', '17', '134'),
(66, 9, '200390', 268, 1, 1, '18', 'золото', '17', '77'),
(67, 10, '100780', 136, 1, 1, '17', 'золото', '3', '65'),
(68, 10, '100785', 137, 2, 1, '17', 'золото', '3', '130'),
(69, 11, '200090', 265, 1, 1, '17', 'золото', '3', '57'),
(70, 12, '101400', 149, 1, 1, '18.5', 'золото', '12', '85'),
(71, 12, '101405', 150, 2, 1, '18.5', 'золото', '12', '134'),
(72, 13, '100780', 136, 1, 1, '17', 'золото', '3', '65'),
(73, 13, '100785', 137, 2, 1, '17', 'золото', '3', '130'),
(74, 13, '200090', 265, 1, 1, '17', 'золото', '3', '57'),
(75, 14, '104540', 247, 1, 1, '18', 'золото', '15', '87'),
(76, 14, '104545', 248, 2, 1, '18', 'золото', '15', '174'),
(77, 14, '102590', 321, 1, 1, '18', 'золото', '1', '67'),
(78, 14, '102595', 322, 2, 1, '18', 'золото', '1', '134'),
(79, 14, '100780', 136, 1, 1, '18', 'золото', '16', '77'),
(80, 14, '100785', 137, 2, 1, '18', 'золото', '16', '154'),
(81, 15, '100100', 122, 1, 1, '19.5', 'золото', '15', '67'),
(82, 15, '100105', 123, 2, 1, '19.5', 'золото', '15', '134'),
(83, 15, '101560', 165, 1, 1, '19.5', 'золото', '15', '62'),
(84, 15, '101565', 166, 2, 1, '19.5', 'золото', '15', '124'),
(85, 16, '101710', 171, 1, 1, '18', 'родий', '7', '77'),
(86, 17, '102590', 321, 1, 1, NULL, 'золото', '1', '67'),
(87, 17, '102595', 322, 2, 1, NULL, 'золото', '1', '134');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `article` varchar(6) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` tinytext NOT NULL,
  `image` varchar(16) NOT NULL DEFAULT 'no_image.png',
  `price_silver` varchar(8) DEFAULT NULL,
  `price_silver2` varchar(8) DEFAULT NULL,
  `price_rodiy` varchar(8) DEFAULT NULL,
  `price_rodiy2` varchar(8) DEFAULT NULL,
  `price_gold` varchar(8) DEFAULT NULL,
  `price_gold2` varchar(8) DEFAULT NULL,
  `is_size` int(1) DEFAULT NULL,
  `is_insert_color` int(1) DEFAULT NULL,
  `type_id` int(1) NOT NULL,
  `collections_id` int(5) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_products_collections_idx` (`collections_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=325 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `article`, `name`, `description`, `image`, `price_silver`, `price_silver2`, `price_rodiy`, `price_rodiy2`, `price_gold`, `price_gold2`, `is_size`, `is_insert_color`, `type_id`, `collections_id`, `status`) VALUES
(120, '100040', '', '', '120.jpg', '65', '90', '72', '97', '72', '97', 1, 1, 1, 11, 1),
(121, '100045', '', '', '121.jpg', '130', '180', '144', '194', '144', '194', 0, 1, 2, 11, 1),
(122, '100100', '', '', '122.jpg', '55', '60', '62', '67', '62', '67', 1, 1, 1, 12, 1),
(123, '100105', '', '', '123.jpg', '110', '120', '124', '134', '124', '134', 0, 1, 2, 12, 1),
(124, '100140', '', '', '124.jpg', '52', '60', '59', '67', '59', '67', 1, 1, 1, 13, 1),
(125, '100145', '', '', '125.jpg', '104', '120', '118', '134', '118', '134', 0, 1, 2, 13, 1),
(126, '100290', '', '', '126.jpg', '60', '60', '67', '67', '67', '67', 1, 0, 1, 14, 1),
(127, '100295', '', '', '127.jpg', '120', '120', '134', '134', '134', '134', 0, 0, 2, 14, 1),
(128, '100300', '', '', '128.jpg', '65', '90', '72', '97', '72', '97', 1, 1, 1, 15, 1),
(129, '100305', '', '', '129.jpg', '130', '180', '144', '194', '144', '194', 0, 1, 2, 15, 1),
(130, '100380', '', '', '130.jpg', '54', '70', '61', '77', '61', '77', 1, 1, 1, 16, 1),
(131, '100385', '', '', '131.jpg', '108', '140', '122', '154', '122', '154', 0, 1, 2, 16, 1),
(132, '100610', '', '', '132.jpg', '60', '75', '67', '82', '67', '82', 1, 1, 1, 17, 1),
(133, '100615', '', '', '133.jpg', '120', '150', '134', '164', '134', '164', 0, 1, 2, 17, 1),
(134, '100620', '', '', '134.jpg', '58', '82', '65', '89', '65', '89', 1, 1, 1, 18, 1),
(135, '100625', '', '', '135.jpg', '116', '164', '130', '178', '130', '178', 0, 1, 2, 18, 1),
(136, '100780', '', '', '136.jpg', '58', '70', '65', '77', '65', '77', 1, 1, 1, 19, 1),
(137, '100785', '', '', '137.jpg', '116', '140', '130', '154', '130', '154', 0, 1, 2, 19, 1),
(138, '100980', '', '', '138.jpg', '57', '68', '64', '75', '64', '75', 1, 1, 1, 20, 1),
(139, '100985', '', '', '139.jpg', '114', '136', '128', '150', '128', '150', 0, 1, 2, 20, 1),
(140, '101140', '', '', '140.jpg', '54', '54', '61', '61', '61', '61', 1, 1, 1, 21, 1),
(141, '101145', '', '', '141.jpg', '108', '108', '122', '122', '122', '122', 0, 0, 2, 21, 1),
(142, '101146', '', '', '142.jpg', '162', '162', '183', '183', '183', '183', 0, 0, 3, 21, 1),
(143, '101190', '', '', '143.jpg', '54', '60', '61', '67', '61', '67', 1, 1, 1, 22, 1),
(144, '101195', '', '', '144.jpg', '108', '120', '122', '134', '122', '134', 0, 1, 2, 22, 1),
(145, '101230', '', '', '145.jpg', '55', '55', '62', '62', '62', '62', 1, 0, 1, 23, 1),
(146, '101235', '', '', '146.jpg', '110', '110', '124', '124', '124', '124', 0, 0, 2, 23, 1),
(147, '101310', '', '', '147.jpg', '56', '70', '63', '77', '63', '77', 1, 1, 1, 24, 1),
(148, '101315', '', '', '148.jpg', '112', '140', '126', '154', '126', '154', 0, 1, 2, 24, 1),
(149, '101400', '', '', '149.jpg', '60', '80', '67', '92', '85', '92', 1, 1, 1, 25, 1),
(150, '101405', '', '', '150.jpg', '120', '170', '134', '184', '134', '184', 0, 1, 2, 25, 1),
(151, '101380', '', '', '151.jpg', '50', '50', '57', '57', '57', '57', 1, 0, 1, 26, 1),
(152, '101385', '', '', '152.jpg', '100', '100', '114', '114', '114', '114', 0, 0, 2, 26, 1),
(153, '101420', '', '', '153.jpg', '58', '75', '65', '82', '65', '82', 1, 1, 1, 27, 1),
(154, '101425', '', '', '154.jpg', '116', '150', '130', '164', '130', '164', 0, 1, 2, 27, 1),
(155, '101460', '', '', '155.jpg', '60', '95', '67', '102', '67', '102', 1, 1, 1, 28, 1),
(156, '101465', '', '', '156.jpg', '120', '190', '134', '204', '134', '204', 0, 1, 2, 28, 1),
(157, '101490', '', '', '157.jpg', '60', '80', '67', '87', '67', '87', 1, 1, 1, 29, 1),
(158, '101495', '', '', '158.jpg', '120', '160', '134', '174', '134', '174', 0, 1, 2, 29, 1),
(159, '101510', '', '', '159.jpg', '70', '90', '77', '97', '77', '97', 1, 1, 1, 30, 1),
(160, '101515', '', '', '160.jpg', '140', '180', '154', '194', '154', '194', 0, 1, 2, 30, 1),
(161, '101520', '', '', '161.jpg', '53', '60', '60', '67', '60', '67', 1, 1, 1, 31, 1),
(162, '101525', '', '', '162.jpg', '106', '120', '120', '134', '120', '134', 0, 1, 2, 31, 1),
(163, '101540', '', '', '163.jpg', '57', '70', '64', '77', '64', '77', 1, 1, 1, 32, 1),
(164, '101545', '', '', '164.jpg', '114', '140', '128', '154', '128', '154', 0, 1, 2, 32, 1),
(165, '101560', '', '', '165.jpg', '55', '55', '62', '62', '62', '62', 1, 0, 1, 33, 1),
(166, '101565', '', '', '166.jpg', '110', '110', '124', '124', '124', '124', 0, 0, 2, 33, 1),
(167, '101600', '', '', '167.jpg', '80', '150', '87', '164', '87', '164', 1, 1, 1, 34, 1),
(168, '101605', '', '', '168.jpg', '160', '300', '174', '314', '174', '314', 0, 1, 2, 34, 1),
(169, '101640', '', '', '169.jpg', '67', '110', '74', '117', '74', '117', 1, 1, 1, 35, 1),
(170, '101645', '', '', '170.jpg', '134', '220', '148', '234', '148', '234', 0, 1, 2, 35, 1),
(171, '101710', '', '', '171.jpg', '70', '90', '77', '97', '77', '97', 1, 1, 1, 36, 1),
(172, '101715', '', '', '172.jpg', '140', '180', '154', '194', '154', '194', 0, 1, 2, 36, 1),
(173, '101910', '', '', '173.jpg', '60', '90', '67', '97', '67', '97', 1, 1, 1, 37, 1),
(174, '101915', '', '', '174.jpg', '120', '180', '134', '194', '134', '194', 0, 1, 2, 37, 1),
(175, '101970', '', '', '175.jpg', '50', '57', '57', '64', '57', '64', 1, 1, 1, 38, 1),
(176, '101975', '', '', '176.jpg', '100', '114', '114', '128', '114', '128', 0, 1, 2, 38, 1),
(177, '102060', '', '', '177.jpg', '60', '70', '67', '77', '67', '77', 1, 1, 1, 39, 1),
(178, '102065', '', '', '178.jpg', '120', '140', '134', '154', '134', '154', 0, 1, 2, 39, 1),
(179, '102070', '', '', '179.jpg', '67', '117', '74', '131', '74', '131', 1, 1, 1, 40, 1),
(180, '102075', '', '', '180.jpg', '134', '234', '148', '248', '148', '248', 0, 1, 2, 40, 1),
(181, '102280', '', '', '181.jpg', '50', '56', '57', '63', '57', '63', 1, 1, 1, 41, 1),
(182, '102285', '', '', '182.jpg', '100', '112', '114', '126', '114', '126', 0, 1, 2, 41, 1),
(184, '102520', '', '', '184.jpg', '60', '80', '67', '87', '67', '87', 1, 1, 1, 43, 1),
(185, '102525', '', '', '185.jpg', '120', '160', '134', '174', '134', '174', 0, 1, 2, 43, 1),
(186, '102540', '', '', '186.jpg', '75', '125', '82', '132', '82', '132', 1, 1, 1, 45, 1),
(187, '102545', '', '', '187.jpg', '150', '250', '164', '264', '164', '264', 0, 1, 2, 45, 1),
(188, '102620', '', '', '188.jpg', '55', '66', '62', '73', '62', '73', 1, 1, 1, 46, 1),
(189, '102625', '', '', '189.jpg', '110', '132', '124', '146', '124', '146', 0, 1, 2, 46, 1),
(190, '102627', '', '', '190.jpg', '55', '66', '62', '73', '62', '73', 0, 1, 4, 46, 1),
(191, '102740', '', '', '191.jpg', '50', '50', '57', '57', '57', '57', 1, 0, 1, 47, 1),
(192, '102745', '', '', '192.jpg', '100', '100', '114', '114', '114', '114', 0, 0, 2, 47, 1),
(193, '102960', '', '', '193.jpg', '60', '116', '67', '123', '67', '123', 1, 1, 1, 48, 1),
(194, '102965', '', '', '194.jpg', '120', '234', '134', '246', '134', '246', 0, 1, 2, 48, 1),
(195, '103050', '', '', '195.jpg', '54', '54', '61', '61', '61', '61', 1, 0, 1, 49, 1),
(196, '103055', '', '', '196.jpg', '108', '108', '122', '122', '122', '122', 0, 0, 2, 49, 1),
(197, '103120', '', '', '197.jpg', '50', '55', '57', '62', '57', '62', 1, 1, 1, 50, 1),
(198, '103125', '', '', '198.jpg', '100', '110', '114', '124', '114', '124', 0, 1, 2, 50, 1),
(199, '103140', '', '', '199.jpg', '60', '100', '67', '107', '67', '107', 1, 1, 1, 51, 1),
(200, '103145', '', '', '200.jpg', '120', '200', '134', '214', '134', '214', 0, 1, 2, 51, 1),
(201, '103220', '', '', '201.jpg', '55', '62', '62', '69', '62', '69', 1, 1, 1, 52, 1),
(202, '103225', '', '', '202.jpg', '110', '124', '124', '138', '124', '138', 0, 1, 2, 52, 1),
(203, '103230', '', '', '203.jpg', '57', '85', '64', '92', '64', '92', 1, 1, 1, 53, 1),
(204, '103235', '', '', '204.jpg', '114', '170', '128', '184', '128', '184', 0, 1, 2, 53, 1),
(205, '103240', '', '', '205.jpg', '60', '90', '67', '97', '67', '97', 1, 1, 1, 54, 1),
(206, '103245', '', '', '206.jpg', '120', '180', '134', '194', '134', '194', 0, 1, 2, 54, 1),
(207, '103280', '', '', '207.jpg', '60', '100', '67', '107', '67', '107', 1, 1, 1, 55, 1),
(208, '103285', '', '', '208.jpg', '120', '200', '134', '214', '134', '214', 1, 1, 1, 55, 1),
(209, '103310', '', '', '209.jpg', '55', '55', '62', '62', '62', '62', 1, 0, 1, 56, 1),
(210, '103315', '', '', '210.jpg', '110', '110', '124', '124', '124', '124', 0, 0, 2, 56, 1),
(211, '103330', '', '', '211.jpg', '65', '100', '72', '107', '72', '107', 1, 1, 1, 57, 1),
(212, '103335', '', '', '212.jpg', '130', '200', '144', '214', '144', '214', 0, 1, 2, 57, 1),
(213, '103320', '', '', '213.jpg', '55', '63', '62', '70', '62', '70', 1, 1, 1, 58, 1),
(214, '103325', '', '', '214.jpg', '110', '126', '124', '140', '124', '140', 0, 1, 2, 58, 1),
(215, '103460', '', '', '215.jpg', '54', '75', '61', '82', '61', '82', 1, 1, 1, 59, 1),
(216, '103465', '', '', '216.jpg', '108', '150', '122', '164', '122', '164', 0, 1, 2, 59, 1),
(217, '103440', '', '', '217.jpg', '60', '60', '67', '67', '67', '67', 1, 0, 1, 60, 1),
(218, '103445', '', '', '218.jpg', '120', '120', '134', '134', '134', '134', 0, 0, 2, 60, 1),
(219, '103370', '', '', '219.jpg', '53', '67', '60', '74', '60', '74', 1, 1, 1, 61, 1),
(220, '103375', '', '', '220.jpg', '106', '134', '120', '148', '120', '148', 0, 1, 2, 61, 1),
(221, '103630', '', '', '221.jpg', '47', '47', '54', '54', '54', '54', 1, 0, 1, 62, 1),
(222, '103635', '', '', '222.jpg', '94', '94', '108', '108', '108', '108', 0, 0, 2, 62, 1),
(223, '103570', '', '', '223.jpg', '55', '90', '62', '97', '62', '97', 1, 1, 1, 63, 1),
(224, '103575', '', '', '224.jpg', '110', '180', '124', '194', '124', '194', 0, 1, 2, 63, 1),
(225, '103660', '', '', '225.jpg', '60', '100', '67', '107', '67', '107', 1, 1, 1, 64, 1),
(226, '103665', '', '', '226.jpg', '120', '200', '134', '214', '134', '214', 0, 1, 2, 64, 1),
(227, '103666', '', '', '227.jpg', '180', '300', '201', '321', '201', '321', 0, 1, 3, 64, 1),
(228, '103640', '', '', 'no_image.png', '55', '70', '62', '77', '62', '77', 1, 1, 1, 65, 1),
(229, '103645', '', '', '229.jpg', '110', '140', '124', '154', '124', '154', 0, 1, 2, 65, 1),
(230, '103710', '', '', '230.jpg', '50', '60', '57', '67', '57', '67', 1, 1, 1, 66, 1),
(231, '103715', '', '', '231.jpg', '100', '120', '114', '134', '114', '134', 0, 1, 2, 66, 1),
(232, '104120', '', '', '232.jpg', '54', '65', '61', '72', '61', '72', 1, 1, 1, 67, 1),
(233, '104125', '', '', '233.jpg', '108', '130', '122', '144', '122', '144', 0, 1, 2, 67, 1),
(234, '104140', '', '', '234.jpg', '80', '150', '87', '157', '87', '157', 1, 1, 1, 68, 1),
(235, '104145', '', '', '235.jpg', '160', '300', '174', '314', '174', '314', 0, 1, 2, 68, 1),
(236, '104250', '', '', '236.jpg', '58', '75', '65', '82', '65', '82', 1, 1, 1, 69, 1),
(237, '104255', '', '', '237.jpg', '116', '150', '130', '164', '130', '164', 0, 1, 2, 69, 1),
(238, '104160', '', '', '238.jpg', '54', '54', '61', '61', '61', '61', 1, 0, 1, 70, 1),
(239, '104165', '', '', '239.jpg', '104', '104', '118', '118', '118', '118', 0, 0, 2, 70, 1),
(240, '104166', '', '', '240.jpg', '162', '162', '183', '183', '183', '183', 0, 0, 3, 70, 1),
(241, '104390', '', '', '241.jpg', '53', '75', '60', '82', '60', '82', 1, 1, 1, 71, 1),
(242, '104395', '', '', '242.jpg', '106', '150', '120', '164', '120', '164', 0, 1, 2, 71, 1),
(243, '104410', '', '', '243.jpg', '55', '75', '62', '82', '62', '82', 1, 1, 1, 72, 1),
(244, '104415', '', '', '244.jpg', '110', '150', '124', '164', '124', '164', 0, 1, 2, 72, 1),
(245, '104460', '', '', '245.jpg', '55', '55', '62', '62', '62', '62', 1, 0, 1, 73, 1),
(246, '104465', '', '', '246.jpg', '110', '110', '124', '124', '124', '124', 0, 0, 2, 73, 1),
(247, '104540', '', '', '247.jpg', '60', '80', '67', '87', '67', '87', 1, 1, 1, 74, 1),
(248, '104545', '', '', '248.jpg', '120', '160', '134', '174', '134', '174', 0, 1, 2, 74, 1),
(249, '104650', '', '', '249.jpg', '55', '60', '62', '67', '62', '67', 1, 1, 1, 75, 1),
(250, '102465', '', '', '250.jpg', '112', '140', '126', '154', '126', '154', 0, 1, 2, 82, 1),
(251, '104730', '', '', '251.jpg', '57', '80', '64', '87', '64', '87', 1, 1, 1, 76, 1),
(252, '104735', '', '', '252.jpg', '114', '160', '128', '174', '128', '174', 0, 1, 2, 76, 1),
(253, '104740', '', '', '253.jpg', '55', '74', '62', '81', '62', '81', 1, 1, 1, 77, 1),
(254, '104855', '', '', '254.jpg', '110', '150', '124', '164', '124', '164', 0, 1, 2, 78, 1),
(255, '104990', '', '', '255.jpg', '55', '75', '62', '82', '62', '82', 1, 1, 1, 79, 1),
(256, '104995', '', '', '256.jpg', '110', '150', '124', '164', '124', '164', 0, 1, 2, 79, 1),
(257, '105060', '', '', '257.jpg', '54', '80', '61', '87', '61', '87', 1, 1, 1, 80, 1),
(258, '105065', '', '', '258.jpg', '108', '160', '122', '174', '122', '174', 0, 1, 2, 80, 1),
(259, '104850', '', '', '259.jpg', '55', '75', '62', '82', '62', '82', 1, 1, 1, 78, 1),
(260, '104745', '', '', '260.jpg', '110', '148', '124', '162', '124', '162', 0, 1, 2, 77, 1),
(261, '200040', '', '', '261.jpg', '52', '60', '59', '67', '59', '67', 1, 1, 1, NULL, 1),
(262, '200050', '', '', '262.jpg', '60', '60', '67', '67', '67', '67', 1, 0, 1, NULL, 1),
(263, '200070', '', '', '263.jpg', '55', '70', '62', '77', '62', '77', 1, 1, 1, NULL, 1),
(264, '200080', '', '', '264.jpg', '60', '110', '67', '117', '67', '117', 1, 1, 1, NULL, 1),
(265, '200090', '', '', '265.jpg', '50', '60', '57', '67', '57', '67', 1, 1, 1, NULL, 1),
(266, '200110', '', '', '266.jpg', '60', '60', '67', '67', '67', '67', 1, 0, 1, NULL, 1),
(267, '200310', '', '', '267.jpg', '50', '65', '57', '72', '57', '72', 1, 1, 1, NULL, 1),
(268, '200390', '', '', '268.jpg', '55', '70', '62', '77', '62', '77', 1, 1, 1, NULL, 1),
(269, '200400', '', '', '269.jpg', '57', '68', '64', '75', '64', '75', 1, 1, 1, NULL, 1),
(270, '200430', '', '', '270.jpg', '60', '100', '67', '107', '67', '107', 1, 1, 1, NULL, 1),
(271, '200440', '', '', '271.jpg', '57', '68', '64', '75', '64', '75', 1, 1, 1, NULL, 1),
(272, '200450', '', '', '272.jpg', '70', '130', '77', '137', '77', '137', 1, 1, 1, NULL, 1),
(273, '200930', '', '', '273.jpg', '54', '75', '61', '82', '61', '82', 1, 1, 1, NULL, 1),
(274, '201280', '', '', '274.jpg', '48', '60', '55', '67', '55', '67', 1, 1, 1, NULL, 1),
(275, '201510', '', '', '275.jpg', '48', '60', '55', '67', '55', '67', 1, 1, 1, NULL, 1),
(276, '201650', '', '', '276.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, 1),
(277, '201770', '', '', '277.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, 1),
(278, '201990', '', '', '278.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, 1),
(279, '202080', '', '', '279.jpg', '50', NULL, '57', NULL, '57', NULL, 1, 1, 1, NULL, 1),
(280, '202090', '', '', '280.jpg', '50', NULL, '57', NULL, '57', NULL, 1, 1, 1, NULL, 1),
(281, '202100', '', '', '281.jpg', '56', '70', '63', '77', '63', '77', 1, 1, 1, NULL, 1),
(282, '202190', '', '', '282.jpg', '55', '75', '62', '82', '62', '82', 1, 1, 1, NULL, 1),
(283, '202210', '', '', '283.jpg', '50', '60', '57', '67', '57', '67', 1, 1, 1, NULL, 1),
(284, '202360', '', '', '284.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, 1),
(285, '202460', '', '', '285.jpg', '60', '70', '67', '77', '67', '77', 1, 1, 1, NULL, 1),
(286, '202680', '', '', '286.jpg', '60', '60', '67', '67', '67', '67', 1, 0, 1, NULL, 1),
(287, '203220', '', '', '287.jpg', '60', '80', '67', '87', '67', '87', 1, 1, 1, NULL, 1),
(288, '203700', '', '', '288.jpg', '53', '67', '60', '74', '60', '74', 1, 1, 1, NULL, 1),
(289, '203730', '', '', '289.jpg', '48', '60', '55', '67', '55', '67', 1, 1, 1, NULL, 1),
(290, '204100', '', '', '290.jpg', '48', '60', '55', '67', '55', '67', 1, 1, 1, NULL, 1),
(291, '204450', '', '', '291.jpg', '75', '150', '82', '157', '82', '157', 1, 1, 1, NULL, 1),
(292, '500140', '', '', '292.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(293, '500150', '', '', '293.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(294, '500160', '', '', '294.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(295, '500190', '', '', '295.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(296, '500250', '', '', '296.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(297, '500260', '', '', '297.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(298, '500270', '', '', '298.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(299, '500300', '', '', '299.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(300, '500320', '', '', '300.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(301, '500470', '', '', '301.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(302, '500770', '', '', '302.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(303, '500800', '', '', '303.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(304, '500810', '', '', '304.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(305, '500860', '', '', '305.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, NULL, 1),
(306, '300050', '', '', '306.jpg', '104', '120', '118', '134', '118', '134', 0, 1, 2, NULL, 1),
(307, '300060', '', '', '307.jpg', '112', '140', '126', '154', '126', '154', 0, 1, 2, NULL, 1),
(308, '300260', '', '', '308.jpg', '100', '120', '114', '134', '114', '134', 0, 1, 2, NULL, 1),
(309, '300280', '', '', '309.jpg', '130', '280', '144', '294', '144', '294', 0, 1, 2, NULL, 1),
(310, '300410', '', '', '310.jpg', '115', '220', '122', '234', '122', '234', 0, 1, 2, NULL, 1),
(311, '300530', '', '', '311.jpg', '100', '110', '114', '124', '114', '124', 0, 1, 2, NULL, 1),
(312, '300460', '', '', '312.jpg', '110', '200', '114', '214', '114', '214', 0, 1, 2, NULL, 1),
(313, '300700', '', '', '313.jpg', '120', '210', '134', '224', '134', '224', 0, 1, 2, NULL, 1),
(314, '300670', '', '', '314.jpg', '120', '210', '134', '224', '134', '224', 0, 1, 2, NULL, 1),
(315, '300350', '', '', '315.jpg', '100', '120', '114', '134', '114', '134', 0, 1, 2, NULL, 1),
(316, '300400', '', '', '316.jpg', '100', '110', '114', '124', '114', '124', 0, 1, 2, NULL, 1),
(317, '300950', '', '', '317.jpg', '110', '150', '124', '164', '124', '164', 0, 1, 2, NULL, 1),
(318, '300690', '', '', '318.jpg', '110', '180', '124', '194', '124', '194', 0, 1, 2, NULL, 1),
(321, '102590', '', '', '321.jpg', '60', '90', '67', '97', '67', '97', 1, 1, 1, 83, 1),
(322, '102595', '', '', '322.jpg', '120', '180', '134', '194', '134', '194', 0, 1, 2, 83, 1),
(323, '102460', '', '', '323.jpg', '56', '70', '63', '77', '63', '77', 1, 1, 1, 82, 1),
(324, '104655', '', '', '324.jpg', '110', '120', '124', '134', '124', '134', 0, 1, 2, 75, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(16) NOT NULL,
  `last_name` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `city` varchar(16) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `permissions` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `city`, `phone`, `permissions`) VALUES
(1, 'Евгений', 'Захарчук', 'ez010286@gmail.com', '3049a1f0f1c808cdaa4fbed0e01649b1', '', '777-77-77', 1),
(2, 'Иван', 'Васильев', 'ez010286@gmail.comm', '2f7b52aacfbf6f44e13d27656ecb1f59', '', '22225585', 0),
(4, 'орморм', 'роморм', 'ppp@ppp.com', '3049a1f0f1c808cdaa4fbed0e01649b1', 'ормор', '45678', 0),
(5, 'nyhgb', 'mhgb', 'ez010286@gmail.cokk', '3049a1f0f1c808cdaa4fbed0e01649b1', 'unyhbg', '417', 0),
(6, 'sfgdhrd', 'Васильев', 'eugen_z86@mail.rut', '3049a1f0f1c808cdaa4fbed0e01649b1', 'Одесса', '22225585', 0),
(7, 'sfgdhrd', 'Васильев', 'ez010286@gmail.chnh', 'be8fe4c12c4e43217c06098a2595a950', 'Одесса', '777-77-77', 0),
(8, 'hhjgjhgjhgh', 'Васильев', 'ez010286@gmail.cci', '3049a1f0f1c808cdaa4fbed0e01649b1', 'kjhlbbkjbkjbhjvm', '777-77-77', 0),
(9, 'sfgdhrd', 'Захарчук', 'ez010286@gmail.jhg', '3049a1f0f1c808cdaa4fbed0e01649b1', 'kjhlbbkjbkjbhjvm', '777-77-77', 0),
(10, 'sfgdhrd', 'Васильев', 'ez286@gmail.com', '3049a1f0f1c808cdaa4fbed0e01649b1', 'Одесса', '777-77-77', 0),
(11, 'sfgdhrd', 'Васильев', 'jj@ppp.com', '3049a1f0f1c808cdaa4fbed0e01649b1', 'Одесса', '777-77-77', 0),
(12, 'Иван', 'Васильев', 'ez06@gmail.com', '3049a1f0f1c808cdaa4fbed0e01649b1', 'Одесса', '22225585', 0),
(13, 'Иван', 'Васильев', 'ppp@phhp.com', '3049a1f0f1c808cdaa4fbed0e01649b1', 'Одесса', '22225585', 0),
(14, 'sfgdhrd', 'Васильев', 'ez010286@gmail.cuuu', '3049a1f0f1c808cdaa4fbed0e01649b1', 'Одесса', '22225585', 0),
(15, 'Иван', 'Васильев', 'ez0286@gmail.com', '3049a1f0f1c808cdaa4fbed0e01649b1', 'Одесса', '22225585', 0),
(16, 'Eugene', 'last_name', 'eugen_z86@mail.ru', '3049a1f0f1c808cdaa4fbed0e01649b1', 'Odessa', '22225585', 0),
(17, 'Тарас', 'Куркин', 'taraskurkin@mail.ru', '3049a1f0f1c808cdaa4fbed0e01649b1', 'Луганск', '+380 (95) 193 71 11', 1),
(18, 'Mike', 'Klimov', 'sd@dff.com', 'd9b1d7db4cd6e70935368a1efb10e377', 'Odessa', '213123123123', 0),
(19, 'Дима', 'Дурак', 'cbs@mail.ru', 'd9b1d7db4cd6e70935368a1efb10e377', 'Одесса', '12314234345', 0),
(20, 'u', 'u', 'ert@mail.ru', 'd9b1d7db4cd6e70935368a1efb10e377', 'киев', '11111', 0),
(21, 'Алена', 'Турчина', 'shok77@yandex.ru', '9e466819f47bb2525213dfa0c3c1b067', 'Луганск', '050 942 02 64', 0),
(22, 'Катерина', 'Булахова', 'kitty_fly@mail.ru', 'db80b9aa37ae77339d4bc2b290d44231', 'Сумы', '0-66-514-92-33', 0),
(23, 'любовь', 'юнусова', 'unik1010@mail.ru', '0b37567a1bda4d5e0be12b95bc8cf71b', 'ровеньки', '+380999329080', 0),
(24, 'светлана', 'бондарь', 'bondar.svetylen@yandex.ua', 'ebf62947b210170bba21ef7e9e3d9ba4', 'луганск', '0991748124', 0),
(25, 'Людмила', 'Лим', 'Lim_lydmila@mail.ru', '730084a3180e86f44f715de81756638a', 'Кривой Рог', '0974162949', 0),
(26, 'Елена', 'Фартак', 'elena.fartak@yandex.ru', '7dd33d61996e180b9d4900cb422893e8', 'Херсон', '0506538989', 0),
(27, 'Каролина', 'Мухина', 'karolina.fly@gmail.com', 'f0e5f568e1487d8d8c8a016b3598f58a', 'Харьков', '0953886302', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
