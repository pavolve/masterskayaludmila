// JavaScript Document

	function viewModal(color, id, set){
	    set = set || 0;
		$('body').prepend("<section id='dark'><div class='buyW'>Загружается....</div></section>");
		$('.buyW').load(
			"/controllers/basket/get_modal.php",
			{id:id, color:color, set:set},
			function(){
	    	    if(set){
	    	        var price = price_static;
	    	        $('#summ').text(price);
	    	        var old_price = price;
	    	        function img_change1(){
	    	            var id = $("select#select_color").val();
	    	            var src = "/img/diamonds/"+images[id];
	    	            $('#select_color_img').attr('src',src);
	    	            if ( mark_up[id] != '0' ){
	    	                price = parseInt(mark_up[id]);
	    	                summ = price*$('input[name=quantity]').val();
	    	                summ = Math.round(summ*100)/100;
	    	                $('#summ').text(summ);
	    	            }
	    	            else{
	    	                $("#colors_message").text("");
	    	                price = old_price;
	    	                $('#summ').text(price);
	    	            }
	    	        }
	    	        img_change1();
	    	        
	    	        $('#select_color').change(
	    	            img_change1
	    	        );
	    	        $('input[name=quantity]').keyup(function(){
	    	            if ($(this).val()){
	    	                summ = price*parseInt($(this).val());
	    	                summ = Math.round(summ*100)/100;
	    	                $('#summ').text(summ);
	    	            }
	    	        })
	    	        $('input[name=quantity]').blur(function(){
	    	            if (!$(this).val())
	    	            $(this).val(1);
	    	            summ = price*parseInt($(this).val());
	    	            summ = Math.round(summ*100)/100;
	    	            $('#summ').text(summ);
	    	        })  
	    	    }
	    	    else{
	    	        var price = $('#summ').text();
	    	        var old_price = price;
	    	        function img_change(){
	    	            var id = $("select#select_color").val();
	    	            var src = "/img/diamonds/"+images[id];
	    	            $('#select_color_img').attr('src',src);
	    	            if ( mark_up[id] != '0' ){
	    	                price = parseInt(mark_up[id]);
	    	                summ = price*$('input[name=quantity]').val();
	    	                summ = Math.round(summ*100)/100;
	    	                $('#summ').text(summ);
	    	            }
	    	            else{
	    	                $("#colors_message").text("");
	    	                price = old_price;
	    	                $('#summ').text(price);
	    	                
	    	            }
	    	        }
	    	        img_change();
	    	        
	    	        $('#select_color').change(
	    	            img_change
	    	        );
	    	        $('input[name=quantity]').keyup(function(){
	    	            if ($(this).val()){
	    	                summ = price*parseInt($(this).val());
	    	                summ = Math.round(summ*100)/100;
	    	                $('#summ').text(summ);
	    	            }
	    	        })
	    	        $('input[name=quantity]').blur(function(){
	    	            if (!$(this).val())
	    	            $(this).val(1);
	    	            summ = price*parseInt($(this).val());
	    	            summ = Math.round(summ*100)/100;
	    	            $('#summ').text(summ);
	    	        })
	    	    }
	    	    $('#submit').unbind('click');
	    	    $('#submit').click(function(){
	    	        var size = $('input[name=size]:checked').val();
	    	        var insert_color = $("select#select_color").val();
	    	        var quantity = $('input[name=quantity]').val();
	    	        var set_id = $('input[name=set_id]').val();
	    	        var product_id = $('input[name=product_id]').val();
	    	        if(set_id){
	    	            $.ajax({
	    	              type: "POST",
	    	              url: "/controllers/basket/add_to_basket.php",
	    	              data: ({set_id : set_id, color : color, size : size, insert_color : insert_color, quantity : quantity}),
	    	              success: function(data){
	    	                $('#basket').load("/controllers/basket/view_basket.php");
	    	                $('.buy').html(data);
	    	                setTimeout('$("#dark").fadeOut(400)', 1000)
	    	              }
	    	            });
	    	        } else if(product_id){
	    	            $.ajax({
	    	              type: "POST",
	    	              url: "/controllers/basket/add_to_basket.php",
	    	              data: ({product_id : product_id, color : color, size : size, insert_color : insert_color, quantity : quantity}),
	    	              success: function(data){
	    	                $('#basket').load("/controllers/basket/view_basket.php");
	    	                $('.buy').html(data);
	    	                setTimeout('$("#dark").fadeOut(400)', 1600)
	    	              }
	    	            });
					}
	    	    });
				$(document).ready(function() {
					var setHeight = $('.buy').height();
					$('.buy').css('margin-top', setHeight/(-2))
				});   
	    	}
		);
	};
    
    
     function basketSubmit(){
         $.ajax({
            url: "/controllers/basket/confirm.php",
            success: function(data){
                $('body').prepend(data);
                $(document).ready(function() {
                    
                    $('#register').live('click',function(){
                        first_name = $('#signup input[name="first_name"]').val();
                        last_name = $('#signup input[name="last_name"]').val();
                        phone = $('#signup input[name="phone"]').val();
                        city = $('#signup input[name="city"]').val();
                        email = $('#signup input[name="email"]').val();
                        password = $('#signup input[name="password"]').val();
                        retype_password = $('#signup input[name="retype_password"]').val();
                        $.post("/controllers/register.php",
                                {first_name : first_name, last_name : last_name, phone : phone, city : city, email : email, password : password, retype_password : retype_password},
                            function(data){
                                if (data == 0){
                                    top.location.href = "../controllers/issue_order.php";
                                }
                                $('.buy').html($(data).children('.buy').html());
                            }
                        )
                        return false;
                    });
                    
                    $('#login').live('click',function(){
                        email = $('#signin input[name="email"]').val();
                        password = $('#signin input[name="password"]').val();
                        $.post("/controllers/login2.php",
                                {email : email, password : password},
                            function(data){
                                if (data == 0){
                                    console.log('redirect');
                                    top.location.href = "../controllers/issue_order.php";
                                }
                                $('.buy').html($(data).children('.buy').html());
                            }
                        )
                        return false;
                    });
                    
                });
            }
        });
    };
    
$(document).ready(function() {
    $('.quantity').keyup(function(){
        var parent = $(this).parents('tr');
        var index = parent.attr('id').slice(1);
        var quantity = parseInt($(this).val());
		parent.find('#price').text('Обновление..');
        $.ajax({
            url: "/controllers/basket/recalculate.php",
            data: ({index : index, quantity : quantity}),
            success: function(data){
                var data = $.parseJSON(data);
                $('#basket_count').text(data['items_count']);
                $('.basket_price').text(data['items_price']);
                parent.find('#price').text(data['price']+' грн');
            }
        });
    });
    $('.quantity').blur(function(){
		if ($(this).val() == ''){$(this).val('0')}
	});
    
}); 
