
		<section id="work-side">
			<h2>Список наборов</h2>
			<section id="collections-list">
            <form method='post' action='/controllers/sets_action.php'>
				<table width="100%">
					<tr>
						<th>Артикул</th>
						<th width="120"></th>
						<th width="15"></th>
					</tr>
                    {foreach $collections as $collection_id => $collection}
					<tr>
						<td>{$collection.article}</td>
						<td><a href='/admin/edit_set/{$collection.id}/'>Редактировать</a></td>
						<td><a href='/controllers/del_set.php?id={$collection.id}/'>X</a></td>
					</tr>
                    {/foreach}
				</table>
				</form>
                
        		{if $total > 1}
        		<nav id="pages">
        			<ul>
        				{$pervpage} {$pageleft[5]} {$pageleft[4]} {$pageleft[3]} {$pageleft[2]} {$pageleft[1]}
        				{$page}
        				{$pageright[1]} {$pageright[2]} {$pageright[3]} {$pageright[4]} {$pageright[5]} {$nextpage} 
        			</ul>
        		</nav>
        		{/if}
			</section>
			
		</section>
		
		<div class="clear"></div>
	</section>
<!-- END CONTENT -->