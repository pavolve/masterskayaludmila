		<aside>
			<h1>Каталог</h1>
			<nav>
				<ul>
					<li><a href="/sets/">Наборы</a></li>
					<li><a href="/rings/">Кольца</a></li>
					<li><a href="/earrings/">Серьги</a></li>
					<li><a href="/bracelets/">Браслеты</a></li>
					<li><a href="/chains/">Колье</a></li>
				</ul>
			</nav>
			<h1>Образцы</h1>
			<nav>
				<ul>
					<li><img src="/img/color-glass.png" alt="Glass"><a href="/diamonds/">Цветные вставки</a></li>
					<li><img src="/img/metals.png" alt="Metals"><a href="/metals/">Виды покрытий</a></li>
				</ul>
			</nav>
			<nav>
				<p><a href="/how-to-buy/"><img src="/img/buy-retail.png" alt="How to buy"></a></p>
			</nav>
		</aside>