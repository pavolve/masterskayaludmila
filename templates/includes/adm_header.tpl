<!DOCTYPE html>
<html>
<head>
	<title>Семейная Мастерская</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="/css/style.css" type="text/css">
</head>
<body>

<section id="container">

<!-- HEADER -->
	<header id="main">
		<section id="logotype">Семейная Мастерская</section>
		<section>
		<h2>Панель администратора</h2>
		<p>Здравствуйте, {$smarty.session.first_name}</p>
		</section>
	</header>
<!-- END HEADER -->