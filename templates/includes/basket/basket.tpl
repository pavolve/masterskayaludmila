<section id="mini-cart">	<header><img src="/img/cart.jpg" alt="Mini Cart"> Корзина</header>
	<section>
	{if !empty($smarty.session.basket)}
		<p><b id="basket_count">{$smarty.session.basket_count}</b> изделий на сумму: <b class="basket_price">{$smarty.session.basket_price}</b> грн</p>
		<a href='/basket'>ЗАГЛЯНУТЬ В КОРЗИНУ</a>
	{else}
		<p style="text-align: center; color: #777;">Ваша корзина сейчас пуста.</p>
	{/if}
	</section>
</section>