

<section id="head-div">Divide</section>

<!-- CONTENT -->
	<section id="content">
		<aside>
			<h1>Меню</h1>
			<nav>
				<ul>
					<li><a href="/admin/new_set/">Добавить новый набор</a></li>
					<li><a href="/admin/new_product/">Добавить новое изделие</a></li>
					<li class="divide">-</li>
					<li><a href="/admin/list/">Список изделий</a></li>
					<li><a href="/admin/sets/">Список наборов</a></li>
					<li><a href="/admin/insert_colors/">Цвета вставок</a></li>
					<li class="divide">-</li>
					<li><a href="/admin/orders/">Заказы</a> ({$order_count})</li>
				</ul>
			</nav>
		</aside>
		