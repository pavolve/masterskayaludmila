<section id="work-side">
	<section id="add-item">
		<h2>{$title}</h2>
		
		<form method='post' action='/controllers/new_set.php?id={$smarty.get.id}' autocomplete='off'>
		
		    <label for="name">Наименование:</label><input type='text' name='name' value='{$values.name}'/>{$err.name}
		
		    <label for="article">Артикул*:</label><input type='text' name='article' value='{$values.article}'/>{$err.article}
		
		    <label for="description">Описание:</label><textarea name='description'>{$values.description}</textarea>{$err.description}
		
		    {if $red == '1'}
		
		    <p><input type='submit' name='submit_red' value='Редактировать' >
		
		    {else}
		
		    <p><input type='submit' name='submit' value='Готово' />
		
		    <input type='submit' name='submit_more' value='Готово, добавить еще' />
		
		    {/if}
		
		</form>
	</section>

		<div class="clear"></div>

</section>