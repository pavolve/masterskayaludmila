<!DOCTYPE html>
<html>
<head>
	<title>Семейная Мастерская</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="/css/style.css" type="text/css" charset="utf-8">
	<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
	<script type='text/javascript' src='/js/scripts.js'></script>
	
<!-- Light View -->
	<!-- Add jQuery library -->
<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script> -->

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
	<link rel="stylesheet" href="/fancybox/source/jquery.fancybox.css?v=2.1.0" type="text/css" media="screen" />
	<script type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js?v=2.1.0"></script>
	
	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.3" type="text/css" media="screen" />
	<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.3"></script>
	<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.3"></script>
	
	<link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.6" type="text/css" media="screen" />
	<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.6"></script>
	{literal}
		<script type="text/javascript">
			$(document).ready(function() {
			    $(".fancybox").fancybox();
			});
		</script>
	{/literal}
</head>
<body>

<section id="hot-line">+380 (95) 193-71-11 ГОРЯЧАЯ ЛИНИЯ</section>
<section id="container">

<!-- HEADER -->
	<header id="main">
		<section id="logotype">Семейная Мастерская</section>
		<article>
			“Уже на протяжении многих лет мы делаем наши украшения
вручную, совмещая старые секреты мастерства с новыми
технологиями. Надеюсь, что наше творчество не оставит
вас равнодушными, а цены приятно удивят”
			<footer>Тарас Куркин</footer>
		</article>
		<section id="basket">
			{include file='includes/basket/basket.tpl'}
		</section>
		<nav class="main-menu">
			<ul>
				<li><a href="/">Главная</a></li>
				<li><a href="/pay-and-delivery/">Доставка &amp; Оплата</a></li>
				<li><a href="/about/">О нас</a></li>
				<li><a href="/contacts/">Контакты</a></li>
                {if $is_logged}
				<li><a href="/logout">Выйти</a></li>
                {else}
                <li><a href="/auth">Войти</a></li>
                {/if}
			</ul>
		</nav>
	</header>
<!-- END HEADER -->