<!-- END HEADER -->

<section id="head-div">Divide</section>

<!-- CONTENT -->
	<section id="content">
		{include file='includes/aside.tpl'}
		
		
		
		<section id="work-side">
{if isset($stage) && $stage == 'new_pass'}
        <form method='post' action='/controllers/password_recovery.php'>
            {$err.recovery}
            <p>Введите новый пароль: <input type='password' name='new_pass' />
            <p>Повторите новый пароль: <input type='password' name='new_pass_retype' />
            <p><input type='hidden' name='hash' value='{$hash}' />
            <p><input type='submit' name='change_pass' value='Восстановить' />
        </form>
{elseif isset($values["recovery_message"])}
        {$values.recovery_message}
{else}
        <form method='post' action='/controllers/password_recovery.php'>
            {$err.recovery}
            <p>Введите Ваш e-mail: <input type='text' name='email' value='{$values.email}'/>
            <p><input type='submit' name='recover' value='Восстановить' />
        </form>
{/if}


</section>

<div class="clear"></div>