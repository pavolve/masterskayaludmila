<section id="head-div">Divide</section>

<!-- CONTENT -->
	<section id="content">
		{include file='includes/aside.tpl'}
		
		<section id="work-side">
			<h1 class="title">Примеры покрытий</h1>
            {section name=foo start=1 loop=148 step=1}
                <div class="item">
                	<a class="fancybox" rel="group" href='/img/metals/{$smarty.section.foo.index}a.jpg'>
    					<img src="/img/metals/{$smarty.section.foo.index}a.jpg" width="320" alt="example">
                	</a>
    				<!-- <p><b>Серебро</b></p> -->
    			</div>
            {/section}
		</section>
		
		<div class="clear"></div>
		
		<section id="why-me">

			<h1>Почему выгодно сотрудничать с нами?</h1>

			<ul class="bg">

				<li><img src="/img/diamond.png" alt="Diamond">Цены это один из самых острых вопросов в бизнесе. Являясь производителями, мы стремимся снизить цены,делая их более привлекательными, чем у конкурентов. Тем более, что большая часть из них не производит, а перепродает.</li>

				<li><img src="/img/diamond.png" alt="Diamond">Наши украшения выполнены вручную из высококачественных гипоаллергенных сплавов. По вашему желанию изделия покрываются золотом серебром или родием.</li>

				<li><img src="/img/diamond.png" alt="Diamond">Так же по желанию заказчика мы можем изменить цвет камня, взгляните на цветовую палитру: <a href="/diamonds/">примеры вставок</a></li>

				<li><img src="/img/diamond.png" alt="Diamond">Производство украшений, для нас, не просто бизнес - это радость общения, вдохновение от сопричастности к прекрасному, возможность делать подарки людям.</li>
				
			</ul>

		</section>
	</section>
<!-- END CONTENT -->