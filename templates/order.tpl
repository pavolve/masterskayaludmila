<section id="head-div">Divide</section>

<!-- CONTENT -->
	<section id="content">
		{include file='includes/aside.tpl'}
		
		<section id="work-side">
			<article>
				<h1>Поздравляем?</h1>
				
				<h2>Ваш заказ был принят</h2>
				
				<p>В течении 6 часов с вами свяжутся для уточнения заказа, если хотите ускорить выполнение заказа, можете связаться с нами по телефону <b>+380 (95) 193-71-11</b> и сообщив номер заказа уточнить стадию его выполнения.</p>
				
				<h3>Ваш заказ номер: <b>{$values.order_message}</b></h3>
								
			</article>
		</section>
		
		<div class="clear"></div>
		
		<!--
<section id="why-me">
			<section class="bg">
				<div class="attention"><img src="/img/attention.png" alt="Внимание"></div>
				<div class="text">Все цены приведенные выше - оптовые. Минимальный заказ от 500 грн. Став нашим партнером вы сможете делать заказы на меньшую сумму.</div>
				<div class="button"><a href="#"><img src="/img/buy-retail.png" alt="Как купить в розницу?"></a></div>
			</section>
		</section>
-->
	</section>
<!-- END CONTENT -->