<!-- POPUP with DARK -->


<!-- END HEADER -->

<section id="head-div">Divide</section>

<!-- CONTENT -->
	<section id="content">
		{include file='includes/aside.tpl'}
		
		<section id="work-side">
            {if !empty($smarty.session.basket)}
			<section id="cart">
				<table width="100%">
					<tr>
						<th>Картинка</th>
						<th>Тип</th>
						<th>Артикул</th>
						<th>Цвет вставки</th>
						<th>Количество</th>
						<th>Цена за шт.</th>
						<th>Цена</th>
						<th></th>
					</tr>
                    {foreach $smarty.session.basket  as $index => $item}
					<tr id='i{$index}'>
						<td><img src="/files/product_images/{$item.image}" width="40" alt="Item"></td>
						<td>{$item.type}</td>
						<td>{$item.article}</td>
						{if $item.insert_color=='None'}
							<td>Нет вставки</td>
						{else}
							<td><img src="/img/diamonds/{$item.insert_color}" width="30" alt="Diamond"></td>
						{/if}
						<td><input class='quantity' type='text' id='item_quantity' maxlength='4' value='{$item.quantity}'></td>
						<td>{$item.cost} грн</td>
						<td><b><span id='price'>{$item.price} грн</span></b></td>
						<td><a href="/controllers/basket/del_from_basket.php?index={$index}">X</a></td>
					</tr>
                    {/foreach}
				</table>
				<section id="summ">Сумма: <b><span id='basket_price' class='basket_price'>{$smarty.session.basket_price}</span> грн</b></section>
				<section id="create-order"><button id='basket_submit' onclick="basketSubmit(); return false;">оформить заказ</button></section>
			</section>
			{else}
				<section style="text-align: center; padding: 20px; border: 1px dashed #aaa; color:#777;">
                	Корзина пуста
				</section>
            {/if}
		</section>
		
		<div class="clear"></div>
		
		<section id="why-me">
			<section class="bg">
				<div class="attention"><img src="/img/attention.png" alt="Внимание"></div>
				<div class="text">Все цены приведенные выше - оптовые. Минимальный заказ от 500 грн. Став нашим партнером вы сможете делать заказы на меньшую сумму.</div>
				<div class="button"><a href="#"><img src="/img/buy-retail.png" alt="Как купить в розницу?"></a></div>
			</section>
		</section>
	</section>
<!-- END CONTENT -->
