<!DOCTYPE html>
<html>
<head>
	<title>Семейная Мастерская</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="/css/style.css" type="text/css">
</head>
<body>

<section id="container">

<!-- HEADER -->
	<header id="main">
		<section id="logotype">Семейная Мастерская</section>
	</header>
<!-- END HEADER -->

<section id="head-div">Divide</section>

<!-- CONTENT -->
	<section id="content">
		<aside>
			<h1>Меню</h1>
			<nav>
				<ul>
					<li><a href="/admin/new_set/">Добавить новый набор</a></li>
					<li><a href="/admin/new_product/">Добавить новое изделие</a></li>
					<li class="divide">-</li>
					<li><a href="/admin/">Список изделий</a></li>
					<li><a href="/admin/sets/">Список наборов</a></li>
					<li><a href="/admin/insert_colors/">Цвета вставок</a></li>
				</ul>
			</nav>
		</aside>
		
		<section id="work-side">
			<h2>Список изделий</h2>
			<section id="cart" class="admin">
                <form method='post' action='/controllers/list_action.php'>
    				<table width="100%">
    					<tr>
    						<th width="15"></th>
    						<th>Картинка</th>
    						<th>Тип</th>
    						<th>Артикул</th>
    						<th>В наборе</th>
    						<th>Вставка</th>
    						<th>Серебро</th>
    						<th>Золото</th>
    						<th>Родий</th>
    						<th>Опубликован</th>
    						<th></th>
    					</tr>
                        {foreach $products as $product_id => $product}
    					<tr>
    						<td><input type='checkbox' name='id[]' value='{$product.id}' /></td>
    						<td><img width="40" alt="Item" src='/files/product_images/{$product.image}' /></td>
    						<td>Кольцо</td>
    						<td>{$product.article}</td>
    						<td>100675</td>
    						<td>Есть</td>
    						<td>{$product.price_silver} грн</td>
    						<td>{$product.price_gold} грн</td>
    						<td>{$product.price_rodiy} грн</td>
    						<td>Да</td>
    						<td><a href='/admin/edit_product/{$product.id}/'>Изменить</a></td>
    					</tr>
                        {/foreach}
    				</table>
				</form>
				<nav id="pages">
        			<ul>
        				{$pervpage} {$pageleft[5]} {$pageleft[4]} {$pageleft[3]} {$pageleft[2]} {$pageleft[1]}
        				{$page}
        				{$pageright[1]} {$pageright[2]} {$pageright[3]} {$pageright[4]} {$pageright[5]} {$nextpage} 
        			</ul>
			    </nav>
				
				<section id="create-order">
					<input type='submit' name='submit' value='Публиковать' />
					<input type='submit' name='submit' value='Не публиковать' />
					<input type='submit' name='submit' value='Удалить' />
				</section>
			</section>
			
		</section>
		
		<div class="clear"></div>
	</section>
<!-- END CONTENT -->

<!-- FOOTER -->
	<footer id="main">
		<nav class="main-menu">
			<ul>
				<li>&copy 2012 Семейная мастерская Людмила - ludmila.com.ua</li>
				<li><a href="#">Выход</a></li>
			</ul>
		</nav>
	</footer>
<!-- END FOOTER -->

</section> <!-- END CONTAINER -->

</body>
</html>